const express = require("express");
const path = require("path");
const cors = require("cors");
const morgan = require("morgan");

const app = express();

//Settings
app.set("port", process.env.PORT || 5000);

//Middlewares
app.use(cors());
app.use(morgan("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

//Public
app.use(express.static(path.join(__dirname, "public")));

//Routes
app.use("/api", require("./routes/task.route"));

module.exports = app;