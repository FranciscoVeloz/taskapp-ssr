const pool = require("../lib/database");
const Task = require("../models/task.model");

const controller = {};

controller.list = async (req, res) => {
  try {
    const data = await pool.query("select * from tasks");
    res.status(302).json(data);
  } catch (error) {
    res.status(404).json({ list: "failed" });
  }
};

controller.listOne = async (req, res) => {
  try {
    const { id } = req.params;
    const data = await pool.query("select * from tasks where id = ?", [id]);
    res.status(302).json(data[0]);
  } catch (error) {
    res.status(404).json({ list: "failed" });
  }
};

controller.insert = async (req, res) => {
  try {
    const { title, description } = req.body;
    const newTask = new Task(title, description);
    await pool.query("insert into tasks set ?", [newTask]);
    res.status(200).json({ insert: "success" });
  } catch (error) {
    res.status(404).json({ insert: "failed" });
  }
};

controller.update = async (req, res) => {
  try {
    const { id } = req.params;
    const { title, description } = req.body;
    const newTask = new Task(title, description);
    await pool.query("update tasks set ? where id = ?", [newTask, id]);
    res.status(200).json({ insert: "success" });
  } catch (error) {
    res.status(404).json({ update: "failed" });
  }
};

controller.delete = async (req, res) => {
  try {
    const { id } = req.params;
    await pool.query("delete from tasks where id = ?", [id]);
    res.status(200).json({ delete: "success" });
  } catch (error) {
    res.status(404).json({ delete: "failed" });
  }
};

module.exports = controller;
