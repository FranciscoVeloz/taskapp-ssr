const { Router } = require('express')
const controller = require('../controllers/task.controller')

const router = Router()

router.get('/task', controller.list)
router.get('/task/:id', controller.listOne)
router.post('/task', controller.insert)
router.put('/task/:id', controller.update)
router.delete('/task/:id', controller.delete)

module.exports = router