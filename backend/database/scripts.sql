create database taskApp;
use taskApp;

drop table tasks;
create table tasks (
    id int primary key auto_increment,
    title varchar(50),
    description varchar(250)
);

insert into tasks values
(null, 'example 1', 'description example 1'),
(null, 'example 2', 'description example 2'),
(null, 'example 3', 'description example 3');