const VERSION = "v1";

const precache = async () => {
  const cache = await caches.open(VERSION);
  const cacheUrls = [
    "/",
    "/index.html",
    "/build/static/css/*.css",
    "/build/static/css/**/*.css",
    "/build/static/js/*.js",
    "/build/static/js/**/*.js",
    "/build/media/**/*.jpg",
    "/build/media/*.jpg",
  ];
  return cache.addAll(cacheUrls);
};

const cachedResponse = async (request) => {
  const cache = await caches.open(VERSION);
  const respone = await cache.match(request);
  return respone || fetch(request);
};

const updateCache = async (request) => {
  const cache = await caches.open(VERSION);
  const response = await fetch(request);
  return cache.put(request, response);
};

self.addEventListener("install", (event) => {
  event.waitUntil(precache());
});

self.addEventListener("fetch", (event) => {
  const request = event.request;

  if (request.method !== "GET") {
    return;
  }

  //Buscar en chache
  event.respondWith(cachedResponse(request));

  //Actualizar cache
  event.waitUntil(updateCache(request));
});
