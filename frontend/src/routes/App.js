import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";

//Importing containers
import Home from "../containers/Home";
import Add from "../containers/Add";
import Edit from "../containers/Edit";

//Importing Layout
import Layout from "../components/Layout";

//Importing assets
import "bootswatch/dist/lux/bootstrap.min.css";
import "sweetalert2/dist/sweetalert2.min.css";
import "../assets/styles/App.css";

const App = () => {
  return (
    <BrowserRouter>
      <Layout>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/add" component={Add} />
          <Route exact path="/edit/:id" component={Edit} />
        </Switch>
      </Layout>
    </BrowserRouter>
  );
};

export default App;
