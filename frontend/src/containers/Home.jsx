import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import Swal from "sweetalert2";

const Home = () => {
  const API = "http://192.168.0.15:5000/api/task";
  const [task, setTask] = useState([]);

  const getTasks = async () => {
    const res = await fetch(API);
    const data = await res.json();
    setTask(data);
  };

  useEffect(() => {
    getTasks();
  }, []);

  const deleteTask = async (id) => {
    await fetch(`${API}/${id}`, {
      method: "DELETE",
    });

    await getTasks();
  };

  const confirmDelete = (id, ttl) => {
    Swal.fire({
      title: `Do you want to delete the task "${ttl}"?`,
      icon: "info",
      showCancelButton: true,
      confirmButtonText: `Delete`,
      confirmButtonColor: "#d9534f",
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire("Deleted!", "", "success");
        deleteTask(id);
      }
    });
  };

  return (
    <div className="container mt-5 mb-5">
      <div className="row">
        <div className="col-lg-10 col-md-12 col-12 mx-auto">
          <div className="table-responsive">
            <table className="table table-striped table-bordered">
              <thead className="bg-primary text-white">
                <tr>
                  <th>Title</th>
                  <th>Description</th>
                  <th>Actions</th>
                </tr>
              </thead>

              <tbody>
                {task.map((t) => (
                  <tr key={t.id}>
                    <td>{t.title}</td>
                    <td>{t.description}</td>
                    <td>
                      <Link to={`/edit/${t.id}`} className="btn btn-info me-2">
                        Edit
                      </Link>
                      <button
                        className="btn btn-danger"
                        onClick={() => {
                          confirmDelete(t.id, t.title);
                        }}
                      >
                        Delete
                      </button>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  tasks: state,
});

export default connect(mapStateToProps, {})(Home);
