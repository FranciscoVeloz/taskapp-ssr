import React, { useState } from "react";
import { useHistory } from "react-router";

const Add = () => {
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");

  const API = "http://192.168.0.15:5000/api/task";
  const history = useHistory();

  const handleSubmit = async (e) => {
    e.preventDefault();
    await fetch(API, {
      method: "POST",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify({
        title,
        description,
      }),
    });

    history.push("/");
  };

  return (
    <div className="container mt-5 mb-5">
      <div className="row">
        <div className="col-lg-4 col-md-6 col-12 mx-auto">
          <div className="card">
            <form onSubmit={handleSubmit} method="POST">
              <div className="card-header pt-3">
                <h4 className="text-center">Add a new task</h4>
              </div>

              <div className="card-body">
                <div className="mb-3">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Title"
                    onChange={(e) => setTitle(e.target.value)}
                    value={title}
                  />
                </div>

                <div className="mb-3">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Description"
                    onChange={(e) => setDescription(e.target.value)}
                    value={description}
                  />
                </div>

                <div className="mb-3 d-grid gap-2">
                  <button type="submit" className="btn btn-primary">
                    Save task
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Add;
